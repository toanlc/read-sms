Đọc tin nhắn gửi đến sau đó hiển thị điểm:

# Cú pháp: 
```
diemthi [mssv]
```
Ví dụ: diemthi 123123

Dữ liệu mẫu:
```
Person p1 = new Person("123123", 6, 7, 8);
Person p2 = new Person("123124", 5, 7, 9);
Person p3 = new Person("123125", 4, 9, 8);
Person p4 = new Person("123126", 3, 7, 3);
Person p5 = new Person("123127", 2, 6, 8);
Person p6 = new Person("123128", 5, 7, 3);
```

# Có thể thêm hoặc chỉnh sửa dữ liệu mẫu trong Person.class.createData();
