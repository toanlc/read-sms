package com.demo.project.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.demo.project.models.Person;

/**
 * Created by TOAN-PC on 12/8/2017.
 */

public class ScoreTableSQL {
    private static final String TABLE_NAME = "table_score";
    private static final String ID = "id";
    private static final String MSSV = "mssv";
    private static final String TOAN = "toan";
    private static final String LY = "ly";
    private static final String HOA = "hoa";

    public static final String TABLE_CREATE = String.format("CREATE TABLE IF NOT EXISTS %s (" +
            "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
            "%s TEXT," +
            "%s INTEGER," +
            "%s INTEGER," +
            "%s INTEGER);", TABLE_NAME, ID, MSSV, TOAN, LY, HOA);

    public static final String TABLE_DROP = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME);

    public static void insert(Context context, Person person) {
        MySQLite.openSession(context);
        MySQLite.beginTran();

        ContentValues values = new ContentValues();
        values.put(MSSV, person.getMssv());
        values.put(TOAN, person.getToan());
        values.put(LY, person.getLy());
        values.put(HOA, person.getHoa());

        MySQLite.writer.insert(TABLE_NAME, null, values);

        MySQLite.endTran();
        MySQLite.closeSession();
    }

    public static Person getScoreByMssv(Context context, String msssv) {
        String sql = String.format("SELECT * FROM %s WHERE %s=%s", TABLE_NAME, MSSV, msssv);

        MySQLite.openSession(context);

        Cursor cursor = MySQLite.reader.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            Person person = new Person();
            person.setMssv(cursor.getString(1));
            person.setToan(cursor.getInt(2));
            person.setLy(cursor.getInt(3));
            person.setHoa(cursor.getInt(4));

            MySQLite.closeSession();

            return person;
        }

        return null;
    }


}
