package com.demo.project.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.demo.project.models.Person;

/**
 * Created by TOAN-PC on 12/8/2017.
 */

public class MySQLite extends SQLiteOpenHelper {
    private static MySQLite sqLite;
    public static SQLiteDatabase reader;
    public static SQLiteDatabase writer;

    private static final String SQL_NAME = "db_score";
    private static final int SQL_VERSION = 1;

    private Context context;

    public MySQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(db);
        onCreate(db);

    }

    private void createTable(SQLiteDatabase db) {
        db.execSQL(ScoreTableSQL.TABLE_CREATE);


    }

    private void dropTable(SQLiteDatabase db) {
        db.execSQL(ScoreTableSQL.TABLE_DROP);
    }

    public static void openSession(Context context) {
        sqLite = new MySQLite(context, SQL_NAME, null, SQL_VERSION);
        reader = sqLite.getWritableDatabase();
        writer = sqLite.getReadableDatabase();
    }

    public static void closeSession() {
        reader.close();
        writer.close();
    }

    public static void beginTran() {
        writer.beginTransaction();
    }

    public static void endTran() {
        writer.setTransactionSuccessful();
        writer.endTransaction();
    }
}
