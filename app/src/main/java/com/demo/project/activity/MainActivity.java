package com.demo.project.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.TextView;

import com.demo.project.R;
import com.demo.project.models.Person;
import com.demo.project.sqlite.ScoreTableSQL;

import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Person.createDBTest(this);

        textView = this.findViewById(R.id.text_message);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        String message = intent.getStringExtra("message");

        if (message!=null){
            String[] value = message.trim().split(" ");

            if (value[0].equals("diemthi")){
                String mssv = value[1];
                Person person = ScoreTableSQL.getScoreByMssv(this, mssv);
                if (person!=null){
                    textView.setText(String.format("MSSV: %s - Diem Toan: %d - Diem Ly: %d - Diem Hoa: %d",
                            mssv, person.getToan(), person.getLy(), person.getHoa()));
                } else {
                    textView.setText("Không tìm thấy");
                }
            }

        }

    }
}
