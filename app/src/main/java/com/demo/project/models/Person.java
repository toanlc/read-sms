package com.demo.project.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.demo.project.sqlite.ScoreTableSQL;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOAN-PC on 12/8/2017.
 */

public class Person {
    private String mssv;
    private int toan;
    private int ly;
    private int hoa;

    public Person() {
    }

    public Person(String mssv, int toan, int ly, int hoa) {
        this.mssv = mssv;
        this.toan = toan;
        this.ly = ly;
        this.hoa = hoa;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public int getToan() {
        return toan;
    }

    public void setToan(int toan) {
        this.toan = toan;
    }

    public int getLy() {
        return ly;
    }

    public void setLy(int ly) {
        this.ly = ly;
    }

    public int getHoa() {
        return hoa;
    }

    public void setHoa(int hoa) {
        this.hoa = hoa;
    }


    public static void createDBTest(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("first", Context.MODE_PRIVATE);

        if (!sharedPreferences.getBoolean("first", false)) {
            List<Person> result = new ArrayList<>();

            Person p1 = new Person("123123", 6, 7, 8);
            result.add(p1);
            Person p2 = new Person("123124", 5, 7, 9);
            result.add(p2);
            Person p3 = new Person("123125", 4, 9, 8);
            result.add(p3);
            Person p4 = new Person("123126", 3, 7, 3);
            result.add(p4);
            Person p5 = new Person("123127", 2, 6, 8);
            result.add(p5);
            Person p6 = new Person("123128", 5, 7, 3);
            result.add(p6);

            for (Person person : result) {
                ScoreTableSQL.insert(context, person);
            }

            sharedPreferences.edit().putBoolean("first", true).apply();

        }

    }
}
